# ---------------------------------------
# Nginx special format analyzer
# Copyright by Vladimir Smagin, 2017
# -----
# Download new version https://bitbucket.org/21h/nginx-csv-log-analyzer/
# Any questions? 21h@blindage.org or http://blindage.org

import csv
from terminaltables import AsciiTable
import argparse
import time

# custom nginx log file:
# log_format compression '"$remote_addr","$time_local","$status","$request";';
# Position  |   Description
# 0         |   IP address
# 1         |   Time stamp
# 2         |   Response code
# 3         |   Request [type url protocol]

# timestamp format in logfile
# datetime.datetime.strptime(ts,'%d/%b/%Y:%H:%M:%S %z')
# user's timestamp
# datetime.datetime.strptime(ts,'%d-%m-%Y %H:%M:%S')


cmdLineParser = argparse.ArgumentParser(description='Get statistics from nginx special format log file')
cmdLineParser.add_argument('--log', metavar='logfile', type=str, required=True,
                           help='Of course, you need to parse something')
cmdLineParser.add_argument('--code', metavar='HTTP_CODE', type=int, help='Show only response code')
cmdLineParser.add_argument('--time', metavar='', type=str, help='Start counting from timestamp DD-MM-YYYY HH:MM:SS')
cmdLineParser.add_argument('--totime', metavar='', type=str, help='Stop counting from timestamp DD-MM-YYYY HH:MM:SS')
cmdLineParser.add_argument('--summ', action='store_true', help='Display fancy summary')
args = cmdLineParser.parse_args()


def summary(userUnixTimeStamp, userUnixTimeStampStop):
    with open(args.log) as nginxLogFile:
        successCounter = 0
        redirectCounter = 0
        clientErrorCounter = 0
        serverErrorCounter = 0
        serverErrorCounter500 = 0
        serverErrorCounter502 = 0
        allRequests = 0
        logStringsIterable = csv.reader(nginxLogFile, delimiter=",", quotechar='"', lineterminator=";")
        for logString in logStringsIterable:
            # Now parse date if not False
            # It's painful
            if userUnixTimeStamp is not False:
                logUnixTimeStamp = time.strptime(logString[1], '%d/%b/%Y:%H:%M:%S %z')
                if userUnixTimeStamp >= logUnixTimeStamp:
                    continue
                if userUnixTimeStampStop < logUnixTimeStamp:
                    break
            allRequests += 1
            if '20' in logString[2]: successCounter += 1
            if '30' in logString[2]: redirectCounter += 1
            if '40' in logString[2]: clientErrorCounter += 1
            if '500' in logString[2]: serverErrorCounter500 += 1
            if '502' in logString[2]: serverErrorCounter502 += 1
            if '50' in logString[2]: serverErrorCounter += 1
    # summary
    tableData = [
        ['HTTP codes', 'Type', 'Count'],
        ['All', '', "{:,}".format(allRequests)],
        ['20x', 'Success', "{:,}".format(successCounter)],
        ['30x', 'Redirection', "{:,}".format(redirectCounter)],
        ['40x', 'Client errors', "{:,}".format(clientErrorCounter)],
        ['500', 'Server errors', "{:,}".format(serverErrorCounter500)],
        ['502', 'Server errors', "{:,}".format(serverErrorCounter502)],
        ['50x', 'Server errors', "{:,}".format(serverErrorCounter)]
    ]
    resultTable = AsciiTable(tableData)
    resultTable.inner_heading_row_border = True
    resultTable.outer_border = False
    resultTable.inner_row_border = False
    print(resultTable.table)


def searchCode(httpCode, userUnixTimeStamp, userUnixTimeStampStop):
    with open(args.log) as nginxLogFile:
        codeCounter = 0
        logStringsIterable = csv.reader(nginxLogFile, delimiter=",", quotechar='"', lineterminator=";")
        for logString in logStringsIterable:
            # Now parse date if not False
            # It's painful, yes, I know, it was earlier and now it must feels easier
            if userUnixTimeStamp is not False:
                logUnixTimeStamp = time.strptime(logString[1], '%d/%b/%Y:%H:%M:%S %z')
                if userUnixTimeStamp >= logUnixTimeStamp:
                    continue
                if userUnixTimeStampStop < logUnixTimeStamp:
                    break
            if str(httpCode) in logString[2]:
                codeCounter += 1
    # print result
    print(codeCounter)


# Set start time point

if args.time:
    unixTimeStamp = time.strptime(args.time, '%d-%m-%Y %H:%M:%S')
else:
    unixTimeStamp = False

# Set stop time point
if args.totime:
    unixTimeStampStop = time.strptime(args.totime, '%d-%m-%Y %H:%M:%S')
else:
    unixTimeStampStop = False

if args.code:
    searchCode(args.code, unixTimeStamp, unixTimeStampStop)

if args.summ:
    summary(unixTimeStamp, unixTimeStampStop)
